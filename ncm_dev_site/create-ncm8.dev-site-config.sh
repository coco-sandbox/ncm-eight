#!/bin/sh

# Create a sites.php file
if [ ! -f ../web/sites/sites.php ]
  then
    cp sites.php ../web/sites
fi

# Create a sites/ncm8.dev folder
if [ ! -d ../web/sites/ncm8.dev ]
  then
    cp -r ncm8.dev ../web/sites
fi

# Create a sites/ncm8.dev folder
if [ ! -d ../config/ncm8.dev ]
  then
    mkdir ../config/ncm8.dev
fi

# Create a sites/ncm8.dev folder
if [ ! -d ../files/ncm8.dev ]
  then
    mkdir ../files/ncm8.dev
fi

# Create a sites/ncm8.dev/private folder
if [ ! -d ../files/ncm8.dev/private ]
  then
    mkdir ../files/ncm8.dev/private
fi

# Create symlinks for custom things
if [ ! -d ../symlinked ]
  then
    mkdir ../symlinked
fi
cd ../symlinked
if [ ! -d profiles ]
  then
    mkdir profiles
    cd profiles
    ln -s ../../web/profiles/contrib/ncm ncm
    cd ..
fi
if [ ! -d themes ]
  then
    mkdir themes
    cd themes
    ln -s ../../web/themes/contrib/ncm_campaign ncm_campaign
    cd ..
fi
cd ../web
drush use ncm8.dev;
drush cc drush;
drush si-ncm-dev;
cd ..

